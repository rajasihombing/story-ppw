# Generated by Django 3.1.2 on 2020-10-13 16:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('matkul', models.CharField(max_length=100)),
                ('dosen', models.CharField(max_length=100)),
                ('sks', models.IntegerField()),
                ('deskripsi', models.CharField(max_length=150)),
                ('semester', models.CharField(max_length=150)),
                ('kelas', models.CharField(help_text='Masukkan tahun semester, e.g 2020/2021', max_length=150)),
            ],
        ),
    ]
