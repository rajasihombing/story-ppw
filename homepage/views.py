from django.shortcuts import render, redirect
from .models import Schedule as jadwal
from .forms import SchedForm

# Create your views here.
def aboutme(request):
    return render(request, 'aboutme.html')

def hobbie(request):
    return render(request, 'hobbie.html')

def home(request):
    return render(request, 'home.html')

def reachme(request):
    return render(request, 'reachme.html')

def join(request):
    if request.method == 'POST':
        form = SchedForm(request.POST)
        if form.is_valid():
            schd = jadwal()
            schd.matkul = form.cleaned_data['matkul']
            schd.dosen = form.cleaned_data['dosen']
            schd.sks = form.cleaned_data['sks']
            schd.deskripsi = form.cleaned_data['deskripsi']
            schd.semester = form.cleaned_data['semester']
            schd.kelas = form.cleaned_data['kelas']
            schd.save() 
        return redirect("/jadwal")

    else:
            schd = jadwal.objects.all()
            form = SchedForm()
            response = {'schd':schd, 'form': form}
            return render(request, 'jadwal.html', response)

def delete(request, pk):
    if request.method == 'POST':
        form = SchedForm(request.POST)
        if form.is_valid():
            schd = jadwal()
            schd.matkul = form.cleaned_data['matkul']
            schd.dosen = form.cleaned_data['dosen']
            schd.sks = form.cleaned_data['sks']
            schd.deskripsi = form.cleaned_data['deskripsi']
            schd.semester = form.cleaned_data['semester']
            schd.kelas = form.cleaned_data['kelas']
            schd.save() 
        return redirect("/jadwal")
    else:
            jadwal.objects.filter(pk = pk).delete()
            data = jadwal.objects.all()
            form = SchedForm()
            response = {
                "schd" : data,
                "form": form
            }
            return render(request, 'jadwal.html', response)