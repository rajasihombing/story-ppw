from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('hobbie/', views.hobbie, name='hobbie'),
    path('reachme/', views.reachme, name='reachme'),
    path('jadwal/', views.join, name='Schedule'),
    path('<int:pk>', views.delete, name = 'delete'),
    # dilanjutkan ...
]